/**
 * 
 */
package com.szuppe.jakub.common;

/**
 * Klasa reprezentuje odcinek w przestrzeni dwuwymiarowe. Niezmienna (immutable).
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class LineSegment2D extends Line2D
{
	/** Współrzędne początku odcinka. */
	private final Coordinates2D	start;
	/** Współrzędne końca odcinka. */
	private final Coordinates2D	end;

	/**
	 * Tworzy odcinek na podstawie dwóch (różnych!) punktów krańcowych
	 * odcinka.
	 * 
	 * @param start - punkt krańcowy odcinka (początek).
	 * @param end - drugi punkt krańcowy odcinka (koniec).
	 */
	public LineSegment2D(final Coordinates2D start, final Coordinates2D end)
	{
		super(start, end);
		this.start = start;
		this.end = end;
	}

	/**
	 * @return Współrzędne początku odcinka.
	 */
	public Coordinates2D getStart()
	{
		return new Coordinates2D(start);
	}

	/**
	 * @return Współrzędne końca odcinka.
	 */
	public Coordinates2D getEnd()
	{
		return new Coordinates2D(end);
	}

	/**
	 * Oblicza i zwraca punkt przecięcia się odcinka z podaną półprostą.
	 * 
	 * @param ray - półprosta
	 * @return Punkt przecięcia się półprostej i odcinka (jeżeli istniej)
	 * @throws DoesnotIntersectException - rzucany, jeżeli nie istnieje punkt przecięcia
	 * między półprostą a odcinkiem.
	 */
	@Override
	public Coordinates2D intersectionPoint(Ray2D ray) throws DoesnotIntersectException
	{
		final float otherLineSlope = ray.getSlope();
		final float otherLineYIntercept = ray.getYIntercept();
		final float thisLineSegmentSlope = getSlope();
		final float thisLineSegmentYIntercept = getYIntercept();

		// slopes are the same, line segments are parallel. They dont intersect.
		if (otherLineSlope == thisLineSegmentSlope)
		{
			throw new DoesnotIntersectException();
		}

		final float intersectionX;
		final float intersectionY;
		final Coordinates2D intersectionPoint;

		boolean isBetweenStartAndEnd = false;
		// pionowa linia
		if (getyCoefficient() == 0)
		{
			intersectionX = -getConstantTerm();
			intersectionY = (ray.getxCoefficient() * intersectionX + ray
					.getConstantTerm()) / -ray.getyCoefficient();
			intersectionPoint = new Coordinates2D(intersectionX, intersectionY);
			isBetweenStartAndEnd = start.getY() <= intersectionY
					&& intersectionY <= end.getY() || end.getY() <= intersectionY
					&& intersectionY <= start.getY();			
		}
		// pozioma lub y = ax + c
		else
		{
			// this segment equation: y = thisLineSegmentSlope * x +
			// thisLineSegmentYIntercept
			// other segment equation: y = otherLineSegmentSlope * x +
			// otherLineSegmentYIntercept
			// solve intersection x = (otherLineSegmentYIntercept -
			// thisLineSegmentYIntercept)/(thisLineSegmentSlope - otherLineSegmentSlope)
			intersectionX = (otherLineYIntercept - thisLineSegmentYIntercept)
					/ (thisLineSegmentSlope - otherLineSlope);
			intersectionY = thisLineSegmentSlope * intersectionX
					+ thisLineSegmentYIntercept;
			intersectionPoint = new Coordinates2D(intersectionX, intersectionY);
			isBetweenStartAndEnd = start.getX() <= intersectionX
					&& intersectionX <= end.getX() || end.getX() <= intersectionX
					&& intersectionX <= start.getX();
		}

		if (isBetweenStartAndEnd && ray.signsAreTheSame(intersectionPoint))
		{
			return intersectionPoint;
		}
		else
		{
			throw new DoesnotIntersectException();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.szuppe.jakub.common.Line2D#toString()
	 */
	@Override
	public String toString()
	{
		return "Start: " + start.toString() + " End: " + end.toString() + " A: "
				+ getxCoefficient() + " B: " + getyCoefficient() + " C: "
				+ getConstantTerm();
	}
}
