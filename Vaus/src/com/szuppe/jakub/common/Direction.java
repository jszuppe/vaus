/**
 * 
 */
package com.szuppe.jakub.common;


/**
 * Reprezentuje kierunek.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public enum Direction
{
	RIGHT,
	LEFT,
	UP,
	DOWN;
}
