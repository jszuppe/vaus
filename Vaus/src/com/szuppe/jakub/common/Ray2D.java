/**
 * 
 */
package com.szuppe.jakub.common;

/**
 * Klasa reprezentująca półprostą w przestrzeni dwuwymiarowej.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Ray2D extends Line2D
{
	/** Punkt, w którym zaczyna się półprosta. */
	private final Coordinates2D	start;
	/** Znak, mówiący jak zachowuje się y na półprostej: 1 rośnie, 0 stałe, -1 maleje. */
	private final float			ySign;
	/** Znak, mówiący jak zachowuje się x na półprostej: 1 rośnie, 0 stałe, -1 maleje. */
	private final float			xSign;

	/**
	 * @param start - punkt,w którym rozpoczyna się półprosta.
	 * @param otherPointOnRay - inny punkt na tej półprostej
	 */
	public Ray2D(final Coordinates2D start, final Coordinates2D otherPointOnRay)
	{
		super(start, otherPointOnRay);
		this.start = start;
		this.xSign = Math.signum(otherPointOnRay.getX() - start.getX());
		this.ySign = Math.signum(otherPointOnRay.getY() - start.getY());
	}

	/**
	 * Sprawdza, czy zadany punkt znajduję się odpowiedniej (tej, w której jest półprosta)
	 * części prostej, której równanie opisuję tą półprostą.
	 * 
	 * @param point - zadany punkt
	 * @return Prawdę, jeżeli zadany punkt
	 */
	public boolean signsAreTheSame(final Coordinates2D point)
	{
		return xSign == Math.signum(point.getX() - start.getX())
				&& ySign == Math.signum(point.getY() - start.getY());
	}

	/**
	 * @return Punkt, w którym rozpoczyna się półprosta.
	 */
	public Coordinates2D getStart()
	{
		return new Coordinates2D(start);
	}

	/* (non-Javadoc)
	 * @see com.szuppe.jakub.common.Line2D#toString()
	 */
	@Override
	public String toString()
	{
		return "Start: " + start.toString() + " A: " + getxCoefficient() + " B: "
				+ getyCoefficient() + " C: " + getConstantTerm() + " xSign: " + xSign
				+ " ySign: " + ySign;
	}
}
