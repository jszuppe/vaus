/**
 * 
 */
package com.szuppe.jakub.common;

/**
 * Klasa reprezentuje linie prostą w przestrzeni dwuwymiarowe. Niezmienna (immutable).
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Line2D
{
	/** Współczynnik przy x. */
	private final float			xCoefficient;
	/** Współczynnik przy y. */
	private final float			yCoefficient;
	/** Wyraz wolny. */
	private final float			constantTerm;

	/**
	 * Tworzy linie na podstawie dwóch dowolnych, różnych (!) punktów.
	 * 
	 * @param pointA - pierwszy punkt na prostej.
	 * @param pointB - drugi punkt. (inny niż pointA!)
	 */
	public Line2D(final Coordinates2D pointA, final Coordinates2D pointB)
	{
		if(pointA.getX() == pointB.getX())
		{
			// prosta typu x - c = 0
			this.yCoefficient = 0;
			this.xCoefficient = 1;
			this.constantTerm = -pointA.getX();
		}
		else if(pointA.getY() == pointB.getY())
		{
			// y - c = 0
			this.yCoefficient = 1;
			this.xCoefficient = 0;
			this.constantTerm = -pointA.getY();
		}
		else
		{
			// ax - y + c = 0
			this.yCoefficient = -1;
			this.xCoefficient = countSlope(pointA, pointB);
			this.constantTerm = countYIntercept(pointA, pointB);
		}
	}

	/**
	 * @param pointA - punkt na prostej.
	 * @param pointB - inny punkt na prostej.
	 * @return Współczynnik kierunkowy prostej przechodzącej przez punkty pointA i pointB
	 */
	public static float countSlope(final Coordinates2D pointA, final Coordinates2D pointB)
	{
		// slope = (y2-y1)/(x2 - x1)
		return (pointB.getY() - pointA.getY())/(pointB.getX() - pointA.getX());
	}
	
	/**
	 * @param pointA - punkt na prostej.
	 * @param pointB - inny punkt na prostej.
	 * @return Rzędną punktu, w którym prosta przecina oś rzędnych.
	 */
	public static float countYIntercept(final Coordinates2D pointA, final Coordinates2D pointB)
	{
		final float slope = countSlope(pointA, pointB);
		// y-intercept = y - slope * x;
		final float yIntercept = pointA.getY() - slope * pointA.getX();
		return yIntercept;
	}
	
	/**
	 * Oblicza i zwraca punkt przecięcia się prostej z podaną półprostą.
	 * 
	 * @param ray - półprosta
	 * @return Punkt przecięcia się półprostej i odcinka (jeżeli istniej)
	 * @throws DoesnotIntersectException - rzucany, jeżeli nie istnieje punkt przecięcia
	 * między półprostą a odcinkiem.
	 */
	public Coordinates2D intersectionPoint(Ray2D ray) throws DoesnotIntersectException
	{
		final float otherLineSlope = ray.getSlope();
		final float otherLineYIntercept = ray.getYIntercept();
		final float thisLineSegmentSlope = getSlope();
		final float thisLineSegmentYIntercept = getYIntercept();

		// slopes are the same, line segments are parallel. They dont intersect.
		if (otherLineSlope == thisLineSegmentSlope)
		{
			throw new DoesnotIntersectException();
		}

		final float intersectionX;
		final float intersectionY;
		final Coordinates2D intersectionPoint;
		
		// pionowa linia
		if (getyCoefficient() == 0)
		{
			intersectionX = -getConstantTerm();
			intersectionY = (ray.getxCoefficient() * intersectionX + ray
					.getConstantTerm()) / -ray.getyCoefficient();
			intersectionPoint = new Coordinates2D(intersectionX, intersectionY);					
		}
		// pozioma lub y = ax + c
		else
		{
			// this segment equation: y = thisLineSegmentSlope * x +
			// thisLineSegmentYIntercept
			// other segment equation: y = otherLineSegmentSlope * x +
			// otherLineSegmentYIntercept
			// solve intersection x = (otherLineSegmentYIntercept -
			// thisLineSegmentYIntercept)/(thisLineSegmentSlope - otherLineSegmentSlope)
			intersectionX = (otherLineYIntercept - thisLineSegmentYIntercept)
					/ (thisLineSegmentSlope - otherLineSlope);
			intersectionY = thisLineSegmentSlope * intersectionX
					+ thisLineSegmentYIntercept;
			intersectionPoint = new Coordinates2D(intersectionX, intersectionY);
		}

		if (ray.signsAreTheSame(intersectionPoint))
		{
			return intersectionPoint;
		}
		else
		{
			throw new DoesnotIntersectException();
		}
	}
	
	/**
	 * @return Współczynnik kierunkowy.
	 */
	public float getSlope()
	{
		return xCoefficient/(-yCoefficient);
	}

	/**
	 * @return Rzędną punktu, w którym prosta przecina oś rzędnych.
	 */
	public float getYIntercept()
	{
		return constantTerm/(-yCoefficient);
	}

	/**
	 * @return Współczynnik przy x w równaniu ogólnym prostej.
	 */
	public float getxCoefficient()
	{
		return xCoefficient;
	}

	/**
	 * @return Współczynnik przy y w równaniu ogólnym prostej.
	 */
	public float getyCoefficient()
	{
		return yCoefficient;
	}

	/**
	 * @return Wyraz wolny w równaniu ogólnym prostej.
	 */
	public float getConstantTerm()
	{
		return constantTerm;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "A: " + getxCoefficient() + " B: " + getyCoefficient() + " C: "+ getConstantTerm();
	}
}
