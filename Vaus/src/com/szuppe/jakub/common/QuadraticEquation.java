/**
 * 
 */
package com.szuppe.jakub.common;


/**
 * Prosty solver równania kwadratowego.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class QuadraticEquation
{
	/**
	 * Oblicza rozwiązania równania a*x^2 + b*x + c = 0.
	 * 
	 * @param a - współczynnik przy x^2
	 * @param b - współczynnik przy x
	 * @param c - wyraz wolny
	 * @return Tablice z rzeczywistymi rozwiązaniami równania.
	 * @throws ImaginaryRootsException - rzucany jeżeli nie ma rzeczywistych rozwiązań równania.
	 */
	public static double[] solve (double a, double b, double c) throws ImaginaryRootsException
	{
		double disc = (b * b) - 4 * a * c;	
		double discSqrt = Math.sqrt(disc);
		if(disc > 0)
		{			
			double roots[] = new double[2];
			roots[0] = (-b + discSqrt) / (2 * a); 
			roots[1] = (-b - discSqrt) / (2 * a); 
			return roots;
		}
		else if(disc == 0)
		{
			double roots[] = new double[1];
			roots[0] = (-b) / (2 * a); 
			return roots;
		}
		else
		{
			throw new ImaginaryRootsException();
		}		
	}
}
