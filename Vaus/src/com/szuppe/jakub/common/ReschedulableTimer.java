/**
 * 
 */
package com.szuppe.jakub.common;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Specjalny timer, na którym można założyć jedno zadanie.
 * Kluczowe jest, że owe zadanie może przekładać (i zmieniać)
 * poprzez użycie metody {@link #reschedule(Runnable, long)}.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class ReschedulableTimer
{
	/** Timer. */
	final private Timer		timer;
	/** Zadanie do wykonania. */
	private Runnable	task;
	/** Opakowanie dla zadania, potrzebne aby timer je przyjął*/
	private TimerTask	timerTask;
	/** Flaga, czy jest założone aktywne zadania na timer*/
	private boolean		isTaskActive;

	/**
	 * Tworzy nowy timer bez zadania.
	 */
	public ReschedulableTimer()
	{
		this.timer = new Timer();
		this.isTaskActive = false;
	}

	/**
	 * Ustawia na zegarze zadanie do wykonania po zadanym czasie.
	 * 
	 * @param runnable - zadanie do wykonania.
	 * @param delay - opóźnienie zadania w milisekundach
	 */
	public void schedule(final Runnable runnable, final long delay)
	{
		task = runnable;
		timerTask = new TimerTask()
		{
			public void run()
			{
				task.run();
			}
		};
		timer.schedule(this.timerTask, delay);
		isTaskActive = true;
	}

	/**
	 * Usuwa zadanie (jeżeli jest) z zegara i ustawia
	 * nowe zadanie do wykonania po zadanym czasie.
	 * 
	 * @param runnable - nowe zadanie do wykonania.
	 * @param delay - nowe opóźnienie.
	 */
	public void reschedule(Runnable runnable, long delay)
	{
		if (isTaskActive == false)
		{
			schedule(runnable, delay);
		}
		else
		{
			timerTask.cancel();
			timerTask = new TimerTask()
			{
				public void run()
				{
					task.run();
				}
			};
			timer.schedule(timerTask, delay);
		}
	}

	/**
	 * Usuwa zadanie do wykonania.
	 */
	public void cancelTask()
	{
		if (isTaskActive == true)
		{
			timerTask.cancel();
		}		
	}
}
