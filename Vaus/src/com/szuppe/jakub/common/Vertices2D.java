/**
 * 
 */
package com.szuppe.jakub.common;

import java.util.LinkedList;
import java.util.List;

/**
 * Klasa reprezentujące zbiór wierzchołków.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class Vertices2D
{
	/** Lista współrzędnych wierzchołków. */
	private final LinkedList<Coordinates2D> verticesList;
	
	/**
	 * Tworzy nowy obiekt {@link Vertices2D} na podstawie
	 * podanego obiektu {@link Vertices2D}.
	 * 
	 * @param vertices
	 */
	public Vertices2D(final Vertices2D vertices)
	{
		this(vertices.getVerticesList());			
	}
	
	/**
	 * Tworzy nowy obiekt {@link Vertices2D} na podstawie
	 * listy współrzędnych.
	 * 
	 * @param verticesList - lista współrzędnych wierzchołków.
	 */
	public Vertices2D(final List<Coordinates2D> verticesList)
	{
		this.verticesList = new LinkedList<>();
		for (Coordinates2D vertex : verticesList)
		{
			this.verticesList.add(new Coordinates2D(vertex));
		}	
	}
		
	/**
	 * @return Wartość najmniejszej współrzędnej poziomej wśród wierzchołków.
	 */
	public float getMinX()
	{
		float minX = verticesList.getFirst().getX();
		for (Coordinates2D vertex : verticesList)
		{
			if(vertex.getX() < minX)
			{
				minX = vertex.getX();
			}
		}
		return minX;
	}
	
	/**
	 * @return Wartość największej współrzędnej poziomej wśród wierzchołków.
	 */
	public float getMaxX()
	{
		float maxX = verticesList.getFirst().getX();
		for (Coordinates2D vertex : verticesList)
		{
			if(vertex.getX() > maxX)
			{
				maxX = vertex.getX();
			}
		}
		return maxX;
	}
	
	/**
	 * @return Wartość najmniejszej współrzędnej pionowej wśród wierzchołków.
	 */
	public float getMinY()
	{
		float minY = verticesList.getFirst().getY();
		for (Coordinates2D vertex : verticesList)
		{
			if(vertex.getY() < minY)
			{
				minY = vertex.getY();
			}
		}
		return minY;
	}
	
	/**
	 * @return Wartość największej współrzędnej pionowej wśród wierzchołków.
	 */
	public float getMaxY()
	{
		float maxY = verticesList.getFirst().getY();
		for (Coordinates2D vertex : verticesList)
		{
			if(vertex.getY() > maxY)
			{
				maxY = vertex.getY();
			}
		}
		return maxY;
	}
	
	/**
	 * @return Listę współrzędnych wierzchołków.
	 */
	public LinkedList<Coordinates2D> getVerticesList()
	{
		return verticesList;
	}

	/**
	 * Przesuwa wszystkie wierzchołki o zadany wektor.
	 * 
	 * @param vector
	 */
	public void addToAll(Coordinates2D vector)
	{
		for (Coordinates2D vertex : verticesList)
		{
			vertex.add(vector);
		}		
	}
}
