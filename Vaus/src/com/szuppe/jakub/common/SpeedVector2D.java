/**
 * 
 */
package com.szuppe.jakub.common;

/**
 * Klasa reprezentująca wektor prędkości w dwuwymiarowej przestrzeni,
 * używając pionowej (ySpeed) i poziomej (xSpeed) składowej wektora.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class SpeedVector2D implements Comparable<SpeedVector2D>
{
	/** Pozioma składowa wektora. */
	private float	xSpeed;
	/** Pionowa składowa wektora. */
	private float	ySpeed;

	/**
	 * Tworzy nowy wektor zgodnie z podanymi składowymi:
	 * poziomą i pionową.
	 * 
	 * @param xSpeed - pozioma składowa wektora.
	 * @param ySpeed - pionowa składowa wektora.
	 */
	public SpeedVector2D(float xSpeed, float ySpeed)
	{
		this.xSpeed = xSpeed;
		this.ySpeed = ySpeed;
	}

	/**
	 * Constructs new vector using other speed vector..
	 * 
	 * @param xSpeed
	 * @param ySpeed
	 */
	public SpeedVector2D(SpeedVector2D speed)
	{
		this.xSpeed = speed.getXSpeed();
		this.ySpeed = speed.getYSpeed();
	}

	/**
	 * Rotuje wektor o zadany kąt w radianach.
	 * 
	 * @param alpha - kąt w radianach.
	 */
	public void rotateVector(final double alpha)
	{
		final double speed = getSpeedValue();
		ySpeed = (float) (speed * Math.sin(alpha));
		xSpeed = (float) (speed * Math.cos(alpha));
	}

	/**
	 * Zwraca wartość bezwzględną wektora prędkości.
	 * 
	 * @return Wartość wektora prędkości.
	 */
	public double getSpeedValue()
	{
		final double xSpeedSquared = Math.pow(xSpeed, 2);
		final double ySpeedSquared = Math.pow(ySpeed, 2);
		final double speed = Math.sqrt(xSpeedSquared + ySpeedSquared);
		return speed;
	}

	/**
	 * Odwraca pionową składową wektora.
	 */
	public void reverseYSpeed()
	{
		ySpeed = -ySpeed;
	}

	/**
	 * Odwraca poziomą składową wektora.
	 */
	public void reverseXSpeed()
	{
		xSpeed = -xSpeed;
	}

	/**
	 * Ustawia wektor na identyczny do podanego.
	 * 
	 * @param speed - zadany wektor prędkości.
	 */
	public void setSpeed(SpeedVector2D speed)
	{
		this.xSpeed = speed.getXSpeed();
		this.ySpeed = speed.getYSpeed();
	}

	/**
	 * Odejmuje od wektora prędkości podany wektor.
	 * 
	 * @param speedVector
	 */
	public void subtract(SpeedVector2D speedVector)
	{
		this.xSpeed = xSpeed - speedVector.getXSpeed();
		this.ySpeed = ySpeed - speedVector.getYSpeed();
	}

	/**
	 * Dodaje do wektora podany wektor.
	 * 
	 * @param speedVector
	 */
	public void add(SpeedVector2D speedVector)
	{
		this.xSpeed = xSpeed + speedVector.getXSpeed();
		this.ySpeed = ySpeed + speedVector.getYSpeed();
	}

	/**
	 * Zwraca wektor, który jest wynikiem dodania
	 * do wektora podanego wektora.
	 * 
	 * @param speedVector
	 * @return Wektor, który jest wynikiem dodania do wektora podanego wektora.
	 */
	public SpeedVector2D getAddition(SpeedVector2D speedVector)
	{
		return new SpeedVector2D(xSpeed + speedVector.getXSpeed(), ySpeed
				+ speedVector.getYSpeed());
	}

	/**
	 * Zwraca wektor, który jest wynikiem odjęcia
	 * do wektora podanego wektora.
	 * 
	 * @param speedVector
	 * @return Wektor, który jest wynikiem odjęcia do wektora podanego wektora.
	 */
	public SpeedVector2D getSubtraction(SpeedVector2D speedVector)
	{
		return new SpeedVector2D(xSpeed - speedVector.getXSpeed(), ySpeed
				- speedVector.getYSpeed());
	}

	/**
	 * Oblicza przemieszczenie po podanym czasie.
	 * 
	 * @return Przemieszczenie po zadanym czasie.
	 */
	public Coordinates2D countDisplacement(long timeInterval)
	{
		final float deltaX = xSpeed * timeInterval;
		final float deltaY = ySpeed * timeInterval;
		return new Coordinates2D(deltaX, deltaY);
	}
	
	/**
	 * Zeruje wektor.
	 */
	public void zero()
	{
		xSpeed = 0;
		ySpeed = 0;
	}

	/**
	 * @return Poziomą składową wektora.
	 */
	public float getXSpeed()
	{
		return xSpeed;
	}

	/**
	 * @return Pionową składową wektora.
	 */
	public float getYSpeed()
	{
		return ySpeed;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SpeedVector2D speedVector)
	{
		if (this == speedVector)
		{
			return 0;
		}
		return Double.compare(getSpeedValue(), speedVector.getSpeedValue());
	}

	@Override
	public String toString()
	{
		return "(" + xSpeed + ", " + ySpeed + ")";
	}	
}
