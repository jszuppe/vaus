/**
 * 
 */
package com.szuppe.jakub.common;

/**
 * Wyjątek rzucany, jeżeli rozwiązania równania są tylko zespolone.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class ImaginaryRootsException extends Exception
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1348273736973149859L;
}
