package com.szuppe.jakub.common;

/**
 * Obiekty ze sobą nie kolidują.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class DoesnotIntersectException extends Exception
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6008858473507082480L;
}