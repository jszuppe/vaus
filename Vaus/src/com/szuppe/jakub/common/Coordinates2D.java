/**
 * 
 */
package com.szuppe.jakub.common;

import java.awt.Point;

/**
 * 
 * Klasa reprezentująca położenie w przestrzeni dwuwymiarowej (x,y). Precyzja: float.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Coordinates2D
{
	/** Położenie w poziomie */
	private float	x;
	/** Położenie w pionie */
	private float	y;

	/**
	 * Tworzy nowy obiekt {@link Coordinates2D} i inicjalizuje jego wartość.
	 * 
	 * @param x
	 *            - położenie w poziomie
	 * @param y
	 *            - położenie w pionie
	 */
	public Coordinates2D(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Tworzy nowy obiekt {@link Coordinates2D} na podstawie podanego obiektu {@link Coordinates2D}.
	 * 
	 * @param coordinates
	 *            - położenie
	 */
	public Coordinates2D(Coordinates2D coordinates)
	{
		this(coordinates.getX(), coordinates.getY());
	}

	/**
	 * @return point - punkt typu {@link Point} stworzony na podstawie koordynatów
	 */
	public Point getIntegerPoint()
	{
		final int intX = (int) x;
		final int intY = (int) y;
		return new Point(intX, intY);
	}

	/**
	 * @return Zwraca współrzędną poziomą (x) położenia zaokrągloną do {@link int}
	 */
	public int getIntX()
	{
		return (int) x;
	}

	/**
	 * @return Zwraca współrzędną pionową (y) położenia zaokrągloną do {@link int}
	 */
	public int getIntY()
	{
		return (int) y;
	}

	/**
	 * @return Zwraca współrzędną poziomą (x) położenia.
	 */
	public float getX()
	{
		return x;
	}

	/**
	 * @return Zwraca współrzędną pionową (y) położenia.
	 */
	public float getY()
	{
		return y;
	}

	/**
	 * Przesuwa położenie (x,y) o podany wektor.
	 * 
	 * @param vector
	 *            - wektor przesunięcia.
	 */
	public void add(Coordinates2D vector)
	{
		this.x += vector.getX();
		this.y += vector.getY();
	}

	/**
	 * Zwraca położenie {@link Coordinates2D}, które jest suma obecnych współrzędnych i wektora przesunięcia.
	 * 
	 * @param vector
	 *            - wektor przesunięcia.
	 * @return Nowe położenie, które jest suma obecnych współrzędnych i wektora przesunięcia.
	 */
	public Coordinates2D moveAlongVector(Coordinates2D vector)
	{
		return new Coordinates2D(getX() + vector.getX(), getY() + vector.getY());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		if (obj == this) return true;
		if (!(obj instanceof Coordinates2D)) return false;

		Coordinates2D coordinates2dToCompare = (Coordinates2D) obj;
		return (x == coordinates2dToCompare.getX() && y == coordinates2dToCompare.getY());
	}
}
