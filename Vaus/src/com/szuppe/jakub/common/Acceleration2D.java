/**
 * 
 */
package com.szuppe.jakub.common;

/**
 * Klasa reprezentująca wektor przyspieszenia w dwuwymiarowej przestrzeni,
 * używając pionowej (yAcc) i poziomej (xAcc) składowej wektora.
 * 
 * Jednostka: float / ms^2 
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Acceleration2D
{
	/** Składowa pozioma przyspieszenia. */
	private float	xAcc;
	/** Składowa pionowa przyspieszenia. */
	private float	yAcc;

	/**
	 * Konstruktor. Tworzy nowy obiekt {@link Acceleration2D} i inicjalizuje podanymi wartościami pionowej i poziomej składowej
	 * przyspieszenia.
	 * 
	 * @param xAcc - pozioma składowa .
	 * @param yAcc - pionowa składowa.
	 */
	public Acceleration2D(final float xAcc, final float yAcc)
	{
		this.xAcc = xAcc;
		this.yAcc = yAcc;
	}

	/**
	 * Konstruktor kopiujący. Tworzy nowy obiekt {@link Acceleration2D} o takich samych wartościach jak podany jako argument obiekt
	 * {@link Acceleration2D}.
	 * 
	 * @param acceleration - przyspieszenie na podstawie, którego tworzymy nowy obiekt.
	 */
	public Acceleration2D(final Acceleration2D acceleration)
	{
		this(acceleration.getxAcc(), acceleration.getyAcc());
	}

	/**
	 * Oblicza wektor przesunięcia na podstawie podanego czasu. Zakładamy, że przyspieszenie jest stałe w podanym czasie.
	 * 
	 * @param timeInterval - okres czasu
	 * @return Wektor przesunięcia
	 */
	public Coordinates2D countDisplacement(long timeInterval)
	{
		final float deltaX = xAcc * timeInterval * timeInterval * 0.5f;
		final float deltaY = yAcc * timeInterval * timeInterval * 0.5f;
		return new Coordinates2D(deltaX, deltaY);
	}

	/**
	 * Ustawia przyspieszenie na takie same wartości, jak ma podany jako argument obiekt {@link Acceleration2D}.
	 * 
	 * @param acceleration - przyspieszenie na podstawie, którego ustawiamy składowe przyspieszenia.
	 */
	public void setAcceleration(final Acceleration2D acceleration)
	{
		setAcceleration(acceleration.getxAcc(), acceleration.getyAcc());
	}

	/**
	 * Ustawia wartości składowych zgodnie z podanymi.
	 * 
	 * @param xAcc - nowa wartość składowej poziomej przyspieszenia.
	 * @param yAcc - nowa wartość składowej pionowej przyspieszenia.
	 */
	public void setAcceleration(final float xAcc, final float yAcc)
	{
		this.xAcc = xAcc;
		this.yAcc = yAcc;
	}

	/**
	 * Ustawia wektor przyspieszenia na skierowany przeciwnie do podanego wektora prędkości. Wartości przyjmuje na podstawie podanego
	 * wektora przyspieszenia.
	 * 
	 * @param acc - wektor przyspieszenia.
	 * @param speedVector - wektor prędkości.
	 */
	public void setAccelerationOppositTo(Acceleration2D acc,
			SpeedVector2D speedVector)
	{
		float oppositeXAcc = -1 * Math.abs(acc.getxAcc())
				* Math.signum(speedVector.getXSpeed());
		float oppositeYAcc = -1 * Math.abs(acc.getyAcc())
				* Math.signum(speedVector.getYSpeed());
		setAcceleration(oppositeXAcc, oppositeYAcc);
	}

	/**
	 * Oblicza zmianę wektora prędkości po podanym czasie.
	 * 
	 * @param timeInterval - czas.
	 * @return Zmiana wektora prędkości po podanym czasie.
	 */
	public SpeedVector2D countSpeedChange(long timeInterval)
	{
		return new SpeedVector2D(xAcc * timeInterval, yAcc * timeInterval);
	}

	/**
	 * Ustawia składową pionową na przeciwną.
	 */
	public void reverseYAcc()
	{
		yAcc = -yAcc;
	}

	/**
	 * Ustawia składową poziomą na przeciwną.
	 */
	public void reverseXAcc()
	{
		xAcc = -xAcc;
	}

	/**
	 * Ustawia wektor przyspieszenia na wektor zerowy.
	 */
	public void zero()
	{
		xAcc = 0;
		yAcc = 0;
	}

	/**
	 * @return True, jeżeli wektor przyspieszenia jest zerowy.
	 */
	public boolean isZero()
	{
		return (xAcc == 0 && yAcc == 0);
	}

	/**
	 * @return Wartość poziomej składowej wektora przyspieszenia.
	 */
	public float getxAcc()
	{
		return xAcc;
	}

	/**
	 * @return Wartość pionowej składowej wektora przyspieszenia.
	 */
	public float getyAcc()
	{
		return yAcc;
	}
}
