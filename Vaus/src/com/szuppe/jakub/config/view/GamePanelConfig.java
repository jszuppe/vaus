/**
 * 
 */
package com.szuppe.jakub.config.view;

import java.awt.Color;
import java.awt.Dimension;

/**
 * Ustawienia widoku planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class GamePanelConfig
{
	/** Konfiguracja piłki. */
	private final BallConfig	ballConfig;
	/** Konfiguracja platformy. */
	private final PaddleConfig	paddleConfig;
	/** Konfiguracja klocków. */
	private final BricksConfig	bricksConfig;
	/** Ścieżka do tła. */
	private final String		pathToBackgroundImage;
	/** Preferowany rozmiar. */
	private final Dimension		preferredSize;
	/** Kolor tła. */
	private final Color			bgColor;
	/** Czas, co który odświeżana jest plansza. */
	private final long			refreshRatio;

	public GamePanelConfig(final BallConfig ballConfig, final PaddleConfig paddleConfig,
			final BricksConfig bricksConfig, final String pathToBackgroundImage,
			final Dimension preferredSize, final Color bgColor, final long refreshRatio)
	{
		this.ballConfig = ballConfig;
		this.paddleConfig = paddleConfig;
		this.bricksConfig = bricksConfig;
		this.pathToBackgroundImage = pathToBackgroundImage;
		this.preferredSize = preferredSize;
		this.bgColor = bgColor;
		this.refreshRatio = refreshRatio;
	}

	/**
	 * @return the ballConfig
	 */
	public BallConfig getBallConfig()
	{
		return ballConfig;
	}

	/**
	 * @return the paddleConfig
	 */
	public PaddleConfig getPaddleConfig()
	{
		return paddleConfig;
	}

	/**
	 * @return the bricksConfig
	 */
	public BricksConfig getBricksConfig()
	{
		return bricksConfig;
	}

	/**
	 * @return the pathToBackgroundImage
	 */
	public String getPathToBackgroundImage()
	{
		return pathToBackgroundImage;
	}

	/**
	 * @return the preferredSize
	 */
	public Dimension getPreferredSize()
	{
		return new Dimension(preferredSize);
	}

	/**
	 * @return the bgColor
	 */
	public Color getBgColor()
	{
		return bgColor;
	}

	/**
	 * @return the refreshRatio
	 */
	public long getRefreshRatio()
	{
		return refreshRatio;
	}

}
