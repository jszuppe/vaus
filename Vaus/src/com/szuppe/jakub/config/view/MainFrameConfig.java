/**
 * 
 */
package com.szuppe.jakub.config.view;

import java.awt.Point;

/**
 * Ustawienie głównego okna programu.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class MainFrameConfig
{
	/** Ustawienia panelu menu. */
	private final MenuPanelConfig	menuConfig;
	/** Konfiguracja panelu do gry (planszy). */
	private final GamePanelConfig	gamePanelConfig;
	/** Tytuł okna. */
	private final String			title;				
	/** Czy można rozszerzać okno.*/
	private final boolean			isResizable;			
	/** Położenie.*/
	private final Point				location;				
	/** Operacja podczas zamknięcia.*/
	private final int				defaultCloseOperation;

	/**
	 * @param menuConfig - ustawienia panelu menu,
	 * @param gamePanelConfig - ustawienia widoku planszy.
	 * @param title - tytuł okna.
	 * @param isResizable - czy rozszerzalne.
	 * @param location - pozycja startowa.
	 * @param defaultCloseOperation
	 */
	public MainFrameConfig(final MenuPanelConfig menuConfig, final GamePanelConfig gamePanelConfig, final String title,
			final boolean isResizable, final Point location, final int defaultCloseOperation)
	{
		this.menuConfig = menuConfig;
		this.gamePanelConfig = gamePanelConfig;
		this.title = title;
		this.isResizable = isResizable;
		this.location = location;
		this.defaultCloseOperation = defaultCloseOperation;
	}

	/**
	 * @return Ustawienia panelu menu.
	 */
	public MenuPanelConfig getMenuConfig()
	{
		return menuConfig;
	}

	/**
	 * @return Ustawienia planszy do gry.
	 */
	public GamePanelConfig getGamePanelConfig()
	{
		return gamePanelConfig;
	}

	/**
	 * @return Tytuł okna.
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @return Prawdę, jeżeli można rozszerzać; fałsz wpp.
	 */
	public boolean isResizable()
	{
		return isResizable;
	}

	/**
	 * @return Położenie startowe okna.
	 */
	public Point getLocation()
	{
		return new Point(location);
	}

	/**
	 * @return Domyślna operacje przy zamykaniu.
	 */
	public int getDefaultCloseOperation()
	{
		return defaultCloseOperation;
	}
}
