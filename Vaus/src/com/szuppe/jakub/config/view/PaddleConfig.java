/**
 * 
 */
package com.szuppe.jakub.config.view;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class PaddleConfig
{
	/** Ścieżka do zdjęcia platformy. */
	private final String	pathToPaddleImg;
	/** Czy włączone podwójne buforowanie. */
	private final boolean	isDoubleBuffered;

	/**
	 * Tworzy konfiguracje platformy.
	 * 
	 * @param pathToPaddleImg - ścieżka do zdj paletki.
	 * @param isDoubleBuffered - czy włączone podwójne buforowanie.
	 */
	public PaddleConfig(final String pathToPaddleImg, final boolean isDoubleBuffered)
	{
		this.pathToPaddleImg = pathToPaddleImg;
		this.isDoubleBuffered = isDoubleBuffered;
	}

	/**
	 * @return the pathToPaddleImg
	 */
	public String getPathToPaddleImg()
	{
		return pathToPaddleImg;
	}

	/**
	 * @return the isDoubleBuffered
	 */
	public boolean isDoubleBuffered()
	{
		return isDoubleBuffered;
	}
}
