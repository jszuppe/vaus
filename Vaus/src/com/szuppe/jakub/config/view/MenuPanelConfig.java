/**
 * 
 */
package com.szuppe.jakub.config.view;

import java.awt.Color;
import java.awt.Dimension;

/**
 * Konfiguracja panelu menu.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class MenuPanelConfig
{
	/** Ścieżka do obrazu tła. */
	private final String	pathToBackgroundImage;
	/** Preferowany rozmiar. */
	private final Dimension	preferredSize;
	/** Kolor tła. */
	private final Color		bgColor;

	/**
	 * @param pathToBackgroundImage - ścieżka do obrazu tła.
	 * @param preferredSize - rozmiar.
	 * @param bgColor - kolor tła.
	 */
	public MenuPanelConfig(final String pathToBackgroundImage, final Dimension preferredSize, final Color bgColor)
	{
		this.pathToBackgroundImage = pathToBackgroundImage;
		this.bgColor = bgColor;
		this.preferredSize = preferredSize;
	}

	/**
	 * @return Ścieżkę do obrazu tła.
	 */
	public String getPathToBackgroundImage()
	{
		return pathToBackgroundImage;
	}

	/**
	 * @return Preferowany rozmiar.
	 */
	public Dimension getPreferredSize()
	{
		return new Dimension(preferredSize);
	}

	/**
	 * @return Kolor tła.
	 */
	public Color getBgColor()
	{
		return bgColor;
	}
}
