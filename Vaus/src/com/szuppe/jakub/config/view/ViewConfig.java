/**
 * 
 */
package com.szuppe.jakub.config.view;


/**
 * Konfiguracja widoku. Zawiera ustawienia
 * poszczególnych okien programu.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class ViewConfig
{
	/** Konfiguracja głównego okna. */
	private final MainFrameConfig	mainFrameConfig;

	/**
	 * Tworzy konfiguracje widoku z podaną
	 * konfiguracją okna głównego.
	 * 
	 * @param mainFrameConfig - konfiguracja głównego okna.
	 */
	public ViewConfig(final MainFrameConfig mainFrameConfig)
	{
		this.mainFrameConfig = mainFrameConfig;
	}

	/**
	 * @return Ustawienie głównego okna.
	 */
	public MainFrameConfig getMainFrameConfig()
	{
		return mainFrameConfig;
	}
}
