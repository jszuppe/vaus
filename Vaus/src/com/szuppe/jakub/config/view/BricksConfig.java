/**
 * 
 */
package com.szuppe.jakub.config.view;

import java.util.HashMap;

import com.szuppe.jakub.mockups.BrickType;

/**
 * Konfiguracja kostek, zawiera
 * informacje o ścieżkach do zdjęć odpowiednich
 * kostek.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class BricksConfig
{
	/** Mapa z odpowiednimi ścieżkami do zdjęć kostek. */
	private final HashMap<BrickType, String>	bricksImgPathMap;

	/**
	 * @param bricksImgPathMap
	 */
	public BricksConfig(final HashMap<BrickType, String> bricksImgPathMap)
	{
		this.bricksImgPathMap = bricksImgPathMap;
	}

	/**
	 * Zwraca ścieżkę do obrazka kostki
	 * zadanego typu.
	 * 
	 * @param brickType - typ kostki.
	 * @return Ścieżkę to zdjęcia zadanego typu kostki,
	 */
	public String getPathToBrickImg(BrickType brickType)
	{
		return bricksImgPathMap.get(brickType);
	}
}
