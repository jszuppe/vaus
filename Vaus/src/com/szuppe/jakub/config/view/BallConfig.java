/**
 * 
 */
package com.szuppe.jakub.config.view;

import java.awt.Color;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class BallConfig
{
	/** Kolor wnętrza piłki. */
	private final Color		insideColor;
	/** Kolor obwodu piłki. */
	private final Color		outsideColor;
	/** Czy włączone podwójne buforowanie. */
	private final boolean	isDoubleBuffered;

	/**
	 * @param insideColor - kolor wnętrza piłki.
	 * @param outsideColor - kolor obwódki.
	 * @param isDoubleBuffered - czy włączone podwójne buforowanie.
	 */
	public BallConfig(final Color insideColor, final Color outsideColor,
			final boolean isDoubleBuffered)
	{
		this.insideColor = insideColor;
		this.outsideColor = outsideColor;
		this.isDoubleBuffered = isDoubleBuffered;
	}

	/**
	 * @return the insideColor
	 */
	public Color getInsideColor()
	{
		return insideColor;
	}

	/**
	 * @return the outsideColor
	 */
	public Color getOutsideColor()
	{
		return outsideColor;
	}

	/**
	 * @return the isDoubleBuffered
	 */
	public boolean isDoubleBuffered()
	{
		return isDoubleBuffered;
	}
}
