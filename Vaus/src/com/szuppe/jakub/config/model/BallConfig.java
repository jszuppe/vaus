package com.szuppe.jakub.config.model;

import com.szuppe.jakub.common.Coordinates2D;
import com.szuppe.jakub.common.SpeedVector2D;

/**
 * Klasa zawierająca ustawienia piłki potrzebne modelowi aplikacji.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class BallConfig
{
	/** Promień piłki. */
	private final int					radius;
	/** Prędkość piłki po odczepieniu się jej od platformy. */
	private final SpeedVector2D			speedAfterRelease;
	/** Początkowe współrzędne piłki. */
	private final Coordinates2D			startCoordinates;

	/**
	 * Tworzy konfiguracje piłki na podstawie podanych wartości.
	 * 
	 * @param radius - promień piłki.
	 * @param speedAfterRelease - prędkość piłki po odczepieniu się jej od platformy
	 * @param startCoordinates - początkowe współrzędne piłki.
	 */
	public BallConfig(final int radius, final SpeedVector2D speedAfterRelease, final Coordinates2D startCoordinates)
	{
		this.radius = radius;
		this.speedAfterRelease = speedAfterRelease;
		this.startCoordinates = startCoordinates;
	}
	
	/**
	 * Tworzy konfiguracje piłki na podstawie podanej konfiguracji.
	 * 
	 * @param ballConfig - podane konfiguracja do skopiowania.
	 */
	public BallConfig(BallConfig ballConfig)
	{
		this(ballConfig.getRadius(), ballConfig.getSpeedAfterRelease(), ballConfig.getStartCoordinates());
	}

	/**
	 * @return Promień.
	 */
	public int getRadius()
	{
		return radius;
	}

	/**
	 * @return Prędkość piłki po odczepieniu się jej od platformy.
	 */
	public SpeedVector2D getSpeedAfterRelease()
	{
		return new SpeedVector2D(speedAfterRelease);
	}

	/**
	 * @return Początkowe współrzędne piłki.
	 */
	public Coordinates2D getStartCoordinates()
	{
		return new Coordinates2D(startCoordinates);
	}
}
