/**
 * 
 */
package com.szuppe.jakub.config.model;

/**
 * Klasa zawierająca konfiguracje modelu.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class ModelConfig
{
	/** Konfiguracja planszy do gry. */
	private final LevelConfig	levelConfig;

	/**
	 * Tworzy {@link ModelConfig} o podanej konfiguracje planszy
	 * do gry.
	 * 
	 * @param levelConfig - konfiguracja planszy do gry.
	 */
	public ModelConfig(final LevelConfig levelConfig)
	{
		this.levelConfig = levelConfig;
	}

	/**
	 * @return Konfigurację planszy do gry.
	 */
	public LevelConfig getLevelConfig()
	{
		return levelConfig;
	}
}
