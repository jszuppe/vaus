/**
 * 
 */
package com.szuppe.jakub.config.model;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class PlayerConfig
{
	/** Początkowa liczba żyć platformy. */
	private final int	livesStartNumber;

	public PlayerConfig(final int livesStartNumber)
	{
		this.livesStartNumber = livesStartNumber;
	}

	/**
	 * @return the livesStartNumber
	 */
	public int getLivesStartNumber()
	{
		return livesStartNumber;
	}
}
