/**
 * 
 */
package com.szuppe.jakub.config.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.szuppe.jakub.common.Acceleration2D;
import com.szuppe.jakub.common.Coordinates2D;
import com.szuppe.jakub.common.SpeedVector2D;

/**
 * Konfiguracja platformy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class PaddleConfig
{
	/** Współrzędne wierzchołków platformy na starcie. */
	private final List<Coordinates2D>	verticesList;
	/** Energia wyrażona w prędkości, którą dostaje platforma, gdy ją przyspieszamy. */
	private final SpeedVector2D			absStepSpeed;
	/** Maksymalna prędkość. */
	private final SpeedVector2D			maxAbsSpeed;
	/** Opóźnienie platformy. */
	private final Acceleration2D		absRetardation;

	/**
	 * Tworzy konfiguracje platformy zgodnie
	 * z podanymi wartościami.
	 * 
	 * @param verticesList - startowe współrzędne wierzchołków platformy.
	 * @param absStepSpeed - energia wyrażona w prędkości, którą dostaje platforma, gdy ją
	 *            przyspieszamy.
	 * @param maxAbsSpeed - maksymalna prędkość platformy.
	 * @param absRetardation - opóźnienie platformy.
	 */
	public PaddleConfig(final List<Coordinates2D> verticesList,
			final SpeedVector2D absStepSpeed, final SpeedVector2D maxAbsSpeed,
			final Acceleration2D absRetardation)
	{
		this.verticesList = verticesList;
		this.absStepSpeed = absStepSpeed;
		this.maxAbsSpeed = maxAbsSpeed;
		this.absRetardation = absRetardation;
	}

	/**
	 * Tworzy kopie podanej konfiguracji.
	 * 
	 * @param paddleConfig - konfiguracja platformy.
	 */
	public PaddleConfig(PaddleConfig paddleConfig)
	{
		this(paddleConfig.getStartVertices(), paddleConfig.getAbsStepSpeed(),
				paddleConfig.getMaxAbsSpeed(), paddleConfig.getAbsRetardation());
	}

	/**
	 * @return Współrzędne wierzchołków platformy na starcie.
	 */
	public List<Coordinates2D> getStartVertices()
	{
		return Collections.unmodifiableList(new LinkedList<>(verticesList));
	}

	/**
	 * @return Energię wyrażona w prędkości, którą dostaje platforma, gdy ją
	 *         przyspieszamy.
	 */
	public SpeedVector2D getAbsStepSpeed()
	{
		return new SpeedVector2D(absStepSpeed);
	}

	/**
	 * @return Maksymalną prędkość platformy.
	 */
	public SpeedVector2D getMaxAbsSpeed()
	{
		return new SpeedVector2D(maxAbsSpeed);
	}

	/**
	 * @return Opóźnienie platformy.
	 */
	public Acceleration2D getAbsRetardation()
	{
		return new Acceleration2D(absRetardation);
	}

}
