/**
 * 
 */
package com.szuppe.jakub.config.model;

import java.awt.Dimension;

/**
 * Klasa zawierająca ustawienia planszy potrzebne modelowi aplikacji.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class LevelConfig
{
	/** Szerokość planszy. */
	private final int			levelWidth;
	/** Wysokość planszy. */
	private final int			levelHeight;
	/** Konfiguracja piłki. */
	private final BallConfig	ballConfig;
	/** Konfiguracja platformy. */
	private final PaddleConfig	paddleConfig;
	/** Konfiguracja gracza. */
	private final PlayerConfig  playerConfig;

	/**
	 * Tworzy obiekt na podstawie podanych wartości konfiguracyjnych.
	 * 
	 * @param levelW - szerokość planszy.
	 * @param levelH - wysokość planszy.
	 * @param ballConfig - konfiguracja piłki.
	 * @param paddleConfig - konfiguracja platformy.
	 * @param playerConfig - konfiguracja ustawień gracza.
	 */
	public LevelConfig(final int levelW, final int levelH, final BallConfig ballConfig,
			final PaddleConfig paddleConfig, final PlayerConfig playerConfig)
	{
		this.levelHeight = levelH;
		this.levelWidth = levelW;
		this.ballConfig = ballConfig;
		this.paddleConfig = paddleConfig;
		this.playerConfig = playerConfig;
	}

	/**
	 * @return Szerokość planszy.
	 */
	public int getLevelWidth()
	{
		return levelWidth;
	}

	/**
	 * @return Wysokosć planszy.
	 */
	public int getLevelHeight()
	{
		return levelHeight;
	}

	/**
	 * @return Konfiguracje piłki.
	 */
	public BallConfig getBallConfig()
	{
		return new BallConfig(ballConfig);
	}

	/**
	 * @return Konfiguracje platformy.
	 */
	public PaddleConfig getPaddleConfig()
	{
		return new PaddleConfig(paddleConfig);
	}

	/**
	 * @return Wymiary planszy.
	 */
	public Dimension getLevelDimension()
	{
		return new Dimension(levelWidth, levelHeight);
	}

	/**
	 * @return the playerConfig
	 */
	public PlayerConfig getPlayerConfig()
	{
		return playerConfig;
	}

}
