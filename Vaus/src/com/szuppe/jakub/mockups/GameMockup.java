/**
 * 
 */
package com.szuppe.jakub.mockups;

/**
 * Makieta gromadząca wszystkie makiety, czyli
 * cały stanu gry potrzebny widokowi do poprawnego
 * wyświetlania.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class GameMockup
{
	/** Makieta piłki. */
	private final BallMockup	ballMockup;
	/** Makieta Platformy. */
	private final PaddleMockup	paddleMockup;
	/** Makieta klocków. */
	private final BricksMockup	bricksMockup;
	/** Makieta poziomu. */
	private final LevelMockup	levelMockup;
	/** Makiet z informacjami o graczu. */
	private final PlayerMockup playerMockup;

	/**
	 * Tworzy nową makietę gry.
	 * 
	 * @param ballMockup - makieta piłki.
	 * @param paddleMockup - makieta platformy.
	 * @param levelMockup - makieta planszy.
	 * @param bricksMockup - makieta z listą makiet klocków.
	 */
	public GameMockup(final BallMockup ballMockup, final PaddleMockup paddleMockup, final LevelMockup levelMockup,
			final BricksMockup bricksMockup, final PlayerMockup playerMockup)
	{
		this.ballMockup = ballMockup;
		this.paddleMockup = paddleMockup;
		this.levelMockup = levelMockup;
		this.bricksMockup = bricksMockup;
		this.playerMockup = playerMockup;
	}

	/**
	 * @return Makietę piłki.
	 */
	public BallMockup getBallMockup()
	{
		return ballMockup;
	}

	/**
	 * @return Makietę platformy.
	 */
	public PaddleMockup getPaddleMockup()
	{
		return paddleMockup;
	}

	/**
	 * @return Makietę planszy.
	 */
	public LevelMockup getLevelMockup()
	{
		return levelMockup;
	}

	/**
	 * @return Makietę klocków.
	 */
	public BricksMockup getBricksMockup()
	{
		return bricksMockup;
	}

	/**
	 * @return the playerMockup
	 */
	public PlayerMockup getPlayerMockup()
	{
		return playerMockup;
	}
}
