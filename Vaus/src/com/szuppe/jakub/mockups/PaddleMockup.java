/**
 * 
 */
package com.szuppe.jakub.mockups;

import com.szuppe.jakub.common.Acceleration2D;
import com.szuppe.jakub.common.Coordinates2D;
import com.szuppe.jakub.common.SpeedVector2D;

/**
 * Makietą platformy. Zawiera informacje dotyczące
 * prędkości, przyspieszenia i pozycji.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class PaddleMockup
{
	/** Współrzędne lewego-górnego roku kwadratu zawierającego platformę. */
	private final Coordinates2D		topLeftCoordinates;
	/** Prędkość. */
	private final SpeedVector2D		speed;
	/** Opóźnienie. */
	private final Acceleration2D	retardation;

	/**
	 * Tworzy nową makietę.
	 * 
	 * @param topLeftCoordinates - współrzędne lewego-górnego roku kwadratu zawierającego platformę.
	 * @param speed - wektor prędkości.
	 * @param retardation - opóźnienie.
	 */
	public PaddleMockup(final Coordinates2D topLeftCoordinates, final SpeedVector2D speed,
			final Acceleration2D retardation)
	{
		this.topLeftCoordinates = topLeftCoordinates;
		this.speed = speed;
		this.retardation = retardation;
	}

	/**
	 * @return Współrzędne lewego-górnego roku kwadratu zawierającego platformę.
	 */
	public Coordinates2D getTopLeftCoordinates()
	{
		return new Coordinates2D(topLeftCoordinates);
	}

	/**
	 * @return Wektor prędkości.
	 */
	public SpeedVector2D getSpeed()
	{
		return new SpeedVector2D(speed);
	}

	/**
	 * @return Wektor przyspieszenia.
	 */
	public Acceleration2D getRetardation()
	{
		return new Acceleration2D(retardation);
	}
	
	/**
	 * @return Wartość współrzędnej poziomej (x).
	 */
	public float getX()
	{
		return topLeftCoordinates.getX();
	}

	/**
	 * @return Wartość współrzędnej pionowej (y).
	 */
	public float getY()
	{
		return topLeftCoordinates.getY();
	}
}
