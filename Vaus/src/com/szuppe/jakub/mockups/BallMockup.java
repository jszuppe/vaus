package com.szuppe.jakub.mockups;

import com.szuppe.jakub.common.Acceleration2D;
import com.szuppe.jakub.common.Coordinates2D;
import com.szuppe.jakub.common.SpeedVector2D;

/**
 * Makieta piłki, zawierająca informacje potrzebne widokowi
 * do wyświetlania stanu piłki.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class BallMockup
{
	/** Współrzędne lewego-górnego roku kwadratu zawierającego piłkę. */
	private final Coordinates2D		topLeftCoordinates;
	/** Promień piłki. */
	private final int	radius;	
	/** Prędkość piłki. */
	private final SpeedVector2D speed;
	/** Przyspieszenie piłki. */
	private final Acceleration2D acceleration;

	/**
	 * Tworzy nową makietę na podstawie podanych wartości.
	 * 
	 * @param topLeftCoordinates - współrzędne lewego-górnego roku kwadratu zawierającego piłkę.
	 * @param radius - promień.
	 * @param speed - wektor prędkości.
	 * @param acceleration - wektor przyspieszenia.
	 */
	public BallMockup(final Coordinates2D topLeftCoordinates, final int radius, final SpeedVector2D speed, final Acceleration2D acceleration)
	{
		this.topLeftCoordinates = topLeftCoordinates;	
		this.radius = radius;		
		this.speed = speed;
		this.acceleration = acceleration;
	}	
	
	/**
	 * @return Wartość współrzędnej poziomej.
	 */
	public float getX()
	{
		return topLeftCoordinates.getX();
	}

	/**
	 * @return Wartość współrzędnej pionowej.
	 */
	public float getY()
	{
		return topLeftCoordinates.getY();
	}

	/**
	 * @return Promień.
	 */
	public int getRadius()
	{
		return radius;
	}

	/**
	 * @return Współrzędne lewego-górnego roku kwadratu zawierającego piłkę.
	 */
	public Coordinates2D getTopLeftCoordinates()
	{
		return new Coordinates2D(topLeftCoordinates);
	}

	/**
	 * @return Wektor prędkości piłki.
	 */
	public SpeedVector2D getSpeed()
	{
		return new SpeedVector2D(speed);
	}

	/**
	 * @return Wektor przyspieszenia piłki.
	 */
	public Acceleration2D getAcceleration()
	{
		return acceleration;
	}
}
