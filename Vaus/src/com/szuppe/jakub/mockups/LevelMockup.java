/**
 * 
 */
package com.szuppe.jakub.mockups;

import java.awt.Dimension;

/**
 * Makieta planszy, zawierająca wymiar planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class LevelMockup
{
	/** Wymiary planszy w modelu. */
	private final Dimension dimension;
	
	/**
	 * Tworzy makietę.
	 */
	public LevelMockup(final Dimension dimension)
	{
		this.dimension = dimension;
	}
		
	/**
	 * @return Szerokość planszy.
	 */
	public int getWidth()
	{
		return dimension.width;
	}

	/**
	 * @return Wysokość planszy.
	 */
	public int getHeight()
	{
		return dimension.height;
	}

	/**
	 * @return Wymiary planszy.
	 */
	public Dimension getDimension()
	{
		return new Dimension(dimension);
	}
}
