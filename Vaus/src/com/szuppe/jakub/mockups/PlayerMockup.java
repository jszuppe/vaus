/**
 * 
 */
package com.szuppe.jakub.mockups;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class PlayerMockup
{
	/** Ilość żyć pozostałych graczowi. */
	private final int	livesLeft;

	public PlayerMockup(final int livesLeft)
	{
		this.livesLeft = livesLeft;
	}

	/**
	 * @return Ilość żyć pozostałych graczowi.
	 */
	public int getLivesLeft()
	{
		return livesLeft;
	}

}
