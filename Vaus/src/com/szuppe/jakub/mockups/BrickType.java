/**
 * 
 */
package com.szuppe.jakub.mockups;

/**
 * Typ klocka.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public enum BrickType
{
	NormalBrick,
	NormalRedBrick;	
}
