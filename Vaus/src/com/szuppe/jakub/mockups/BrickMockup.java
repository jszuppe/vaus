/**
 * 
 */
package com.szuppe.jakub.mockups;

import com.szuppe.jakub.common.Coordinates2D;

/**
 * Makieta reprezentująca klocek do zbijania.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class BrickMockup
{
	/** Współrzędne lewego-górnego rogu klocka. */
	private final Coordinates2D	topLeftCoordinates;
	/** Typ klocka. */
	private final BrickType		brickType;

	/**
	 * Tworzy makietę klocka na podstawie pozycji
	 * lewego-górnego rogu oraz typu klocka.
	 * 
	 * @param position - współrzędne lewego-górnego rogu klocka.
	 * @param brickType - typ.
	 */
	public BrickMockup(Coordinates2D position, BrickType brickType)
	{
		this.topLeftCoordinates = position;
		this.brickType = brickType;
	}

	/**
	 * @return Wartość współrzędnej poziomej (x).
	 */
	public float getX()
	{
		return topLeftCoordinates.getX();
	}

	/**
	 * @return Wartość współrzędnej pionowej (y).
	 */
	public float getY()
	{
		return topLeftCoordinates.getY();
	}

	/**
	 * @return Współrzędne lewego-prawego rogu klocka.
	 */
	public Coordinates2D getPosition()
	{
		return new Coordinates2D(topLeftCoordinates);
	}

	/**
	 * @return Typ klocka.
	 */
	public BrickType getBrickType()
	{
		return brickType;
	}
}
