/**
 * 
 */
package com.szuppe.jakub.mockups;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Makieta reprezentująca zbiór klocków.
 * Zawiera wszystkie makiety wszystkich klocków.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class BricksMockup
{
	/** Lista makiet klocków. */
	private final List<BrickMockup> bricksList;
	
	/**
	 * Tworzy pustą makietę.
	 */
	public BricksMockup()
	{
		bricksList = new LinkedList<>();
	}
	
	/**
	 * Dodaje makietę klocka do listy.
	 * 
	 * @param brickMockup - makieta jednego klocka.
	 */
	public void add(BrickMockup brickMockup)
	{
		bricksList.add(brickMockup);
	}

	/**
	 * @return Listę makiet klocków. (unmodifiable)
	 */
	public List<BrickMockup> getBricksList()
	{
		return Collections.unmodifiableList(bricksList);
	}
}
