/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie, mówiące o tym, że należy wczytać kolejny poziom gry.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class LoadNextLevelEvent extends GameEvent
{

}
