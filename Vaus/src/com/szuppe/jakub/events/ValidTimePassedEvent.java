/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie, informujące, że minął czas przez który stan
 * modelu był poprawny. Należy przeliczyć nowy stan.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class ValidTimePassedEvent extends GameEvent
{

}
