/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie gry, tj. takie, które wydarzyło się podczas rozgrywki.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class GameEvent extends AppEvent
{

}
