/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie, mówiące o tym, że gracz przegrał.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class GameLostEvent extends GameEvent
{

}
