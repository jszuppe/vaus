/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie, informujące o tym, że gracz chce rozpocząc grę.
 * Przejść z menu do gry.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class StartGameEvent extends NonGameEvent
{
	
}
