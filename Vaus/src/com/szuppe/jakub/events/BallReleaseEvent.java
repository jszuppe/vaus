/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie informuje, że gracz chce wypuścić piłkę.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class BallReleaseEvent extends GameEvent
{
	
}
