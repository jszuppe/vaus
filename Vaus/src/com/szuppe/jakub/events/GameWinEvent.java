/**
 * 
 */
package com.szuppe.jakub.events;

/**
 * Zdarzenie, mówiące o tym, że gracz wygrał.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class GameWinEvent extends GameEvent
{

}
