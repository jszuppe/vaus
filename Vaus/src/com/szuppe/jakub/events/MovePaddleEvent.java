/**
 * 
 */
package com.szuppe.jakub.events;

import com.szuppe.jakub.common.Direction;

/**
 * Zdarzenie, informujące, że użytkownik chce, aby platform poruszyła się w zadanym
 * kierunku.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class MovePaddleEvent extends GameEvent
{
	/** Kierunek, w który ma poruszyć się platforma.*/
	private final Direction	direction;

	/**
	 * @param direction - kierunek, w który ma poruszyć się platforma.
	 */
	public MovePaddleEvent(final Direction direction)
	{
		this.direction = direction;
	}

	/**
	 * @return Kierunek, w który ma poruszyć się platforma.
	 */
	public Direction getDirection()
	{
		return direction;
	}
}
