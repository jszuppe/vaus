package com.szuppe.jakub.events;

/**
 * Zdarzenie, które zaszło poza rozgrywką.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class NonGameEvent extends AppEvent
{

}
