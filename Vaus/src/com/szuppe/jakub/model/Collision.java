/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * Abstrakcyjna klasa reprezentująca kolizję między obiektami gry.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
abstract class Collision implements Comparable<Collision>
{
	/** Czas po którym nastąpi kolizja. */
	private final long	timeToCollision;

	/**
	 * Tworzy nowy obiekt kolizji, która
	 * nastąpi po podanym czasie,
	 * 
	 * @param timeToCollision - czas do kolizji.
	 */
	public Collision(final long timeToCollision)
	{
		this.timeToCollision = timeToCollision;
	}

	/**
	 * @return Czas do kolizji.
	 */
	public long getTimeToCollision()
	{
		return timeToCollision;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Collision collision)
	{
		return Float.compare(timeToCollision, collision.getTimeToCollision());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		if (obj == this) return true;
		if (!(obj instanceof Collision)) return false;

		Collision collision = (Collision) obj;
		return (Float.compare(timeToCollision, collision.getTimeToCollision()) == 0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.getClass().toString() + " timeToCollision: " + timeToCollision;
	}
}

/**
 * Klasa reprezentująca kolizje między planszą,
 * a piłką. Piłką zderza się z prawą ścianą planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class BallWithLevelRightSideCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do kolizji.
	 */
	public BallWithLevelRightSideCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentująca kolizje między planszą,
 * a piłką. Piłką zderza się z górną ścianą planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class BallWithLevelTopCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do kolizji.
	 */
	public BallWithLevelTopCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentująca kolizje między planszą,
 * a piłką. Piłką zderza się z lewą ścianą planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class BallWithLevelLeftSideCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do kolizji.
	 */
	public BallWithLevelLeftSideCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentująca kolizje między planszą,
 * a piłką. Piłką zderza się z dołem planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class BallWithLevelBottomCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do kolizji.
	 */
	public BallWithLevelBottomCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentująca kolizje między platformą
 * a piłką.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class BallWithPaddleCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do kolizji.
	 */
	public BallWithPaddleCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentująca kolizje między piłą 
 * a klockiem.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class BallWithBrickCollision extends Collision
{
	/** Klocek, z którym zachodzi kolizja.*/
	private final Brick	brick;
	/** Ściana klocka, z którą zderzy się piłka.*/
	private final Side	side;

	/**
	 * @param timeToCollision - czas do kolizji.
	 * @param brick - klocek, z którym zachodzi kolizja.
	 * @param side - ściana klocka, z którą zderzy się piłka.
	 */
	public BallWithBrickCollision(long timeToCollision, Brick brick, Side side)
	{
		super(timeToCollision);
		this.brick = brick;
		this.side = side;
	}

	/**
	 * @return Klocek, z którym zachodzi kolizja.
	 */
	public Brick getBrick()
	{
		return brick;
	}

	/**
	 * @return Ściana klocka, z którą zderzy się piłka.
	 */
	public Side getSide()
	{
		return side;
	}

	/* (non-Javadoc)
	 * @see com.szuppe.jakub.model.Collision#toString()
	 */
	@Override
	public String toString()
	{
		return this.getClass().toString() + " timeToCollision: " + getTimeToCollision()
				+ " side: " + getSide();
	}
}

/**
 * Klasa reprezentująca kolizje między paletką
 * a prawą ścianą planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class PaddleWithLevelRightSideCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do zderzenia/
	 */
	public PaddleWithLevelRightSideCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentująca kolizje między paletką
 * a lewą ścianą planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class PaddleWithLevelLeftSideCollision extends Collision
{
	/**
	 * @param timeToCollision - czas do zderzenia.
	 */
	public PaddleWithLevelLeftSideCollision(long timeToCollision)
	{
		super(timeToCollision);
	}
}

/**
 * Klasa reprezentuje zatrzymanie się 
 * paletki.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
class PaddleWillStop extends Collision
{
	/**
	 * @param timeToCollision - czas do zatrzymania.
	 */
	public PaddleWillStop(long timeToCollision)
	{
		super(timeToCollision);
	}
}