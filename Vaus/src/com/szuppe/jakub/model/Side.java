/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
enum Side
{
	LEFT,
	RIGHT,
	TOP,
	BOTTOM,
	CORNER;
}
