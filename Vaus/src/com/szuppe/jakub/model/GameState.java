/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * Enum reprezentujący stan gry.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public enum GameState
{
	GAME_MENU,
	GAME_STARTED;
}
