/**
 * 
 */
package com.szuppe.jakub.model;

import com.szuppe.jakub.config.model.PlayerConfig;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
class Player
{
	/** Życia, które zostały graczowi. */
	private final Lives lives;
	
	/**
	 * Tworzy na podstawie podanej konfiguracji
	 * nowy obiekt zawierający informacje
	 * o graczu.
	 * 
	 * @param playerConfig
	 */
	public Player(final PlayerConfig playerConfig)
	{
		this.lives = new Lives(playerConfig.getLivesStartNumber());
	}
	
	/**
	 * Gracz stracił życie.
	 */
	public void lostLife()
	{
		lives.dec();
	}
	
	/**
	 * @return Prawdę, jeżeli graczowi pozostały życia; fałsz wpp.
	 */
	public boolean isAlive()
	{
		return lives.getValue() > 0;
	}

	/**
	 * @return Ilość pozostałych żyć.
	 */
	public int getLives()
	{
		return lives.getValue();
	}
}

class Lives
{
	/** Liczba żyć. */
	private int	value;

	/**
	 * Tworzy życie z podaną liczbą żyć.
	 * 
	 * @param livesStartNumber - początkowa liczba żyć.
	 */
	public Lives(final int livesStartNumber)
	{
		this.value = livesStartNumber;
	}

	/**
	 * Odejmuje życie.
	 */
	public void dec()
	{
		value--;
	}

	/**
	 * @return Liczbę pozostałych żyć.
	 */
	public int getValue()
	{
		return value;
	}
}