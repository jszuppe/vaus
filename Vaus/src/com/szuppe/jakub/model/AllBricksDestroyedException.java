/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * Wyjątek rzucany, kiedy wszystkie klocki na bieżącym poziomie zostały zbite.
 * 
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class AllBricksDestroyedException extends Exception
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -6230145191027453790L;

}
