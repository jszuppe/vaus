/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * Wyjątek generowany, kiedy nie ma już kolejnych poziomów gry.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class NoMoreMapsException extends Exception
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 743920389287255213L;
}
