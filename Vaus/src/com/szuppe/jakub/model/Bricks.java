/**
 * 
 */
package com.szuppe.jakub.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

/**
 * Klasa reprezentująca zbiór klocków na planszy.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
class Bricks
{
	/** Lista klocków. */
	private final List<Brick>	bricksList;

	/**
	 * Tworzy nowy obiekt {@link Bricks} na podstawie
	 * podaje listy klocków.
	 * 
	 * @param bricksList - lista kloców.
	 */
	public Bricks(List<Brick> bricksList)
	{
		this.bricksList = new LinkedList<>();
		for (Brick brick : bricksList)
		{
			this.bricksList.add(new Brick(brick));
		}
	}

	/**
	 * Usuwa podany klocek ze zbioru klocków.
	 * Jeżeli nie występuję - nic się nie dzieje.
	 * 
	 * @param brickToRemove - klocek do usunięcia.
	 */
	public void removeBrick(Brick brickToRemove)
	{
		for (Brick brick : bricksList)
		{
			if (brickToRemove == brick)
			{
				bricksList.remove(brick);
				return;
			}
		}
	}

	/**
	 * Sprawdza, czy i kiedy zajadą kolizję między piłką,
	 * a klockami. Jeżeli tak, to zwraca odpowiednią kolekcję
	 * kolizji. Jeżeli nie, zwraca pustą kolekcję.
	 * 
	 * @param ball - piłką, z którą mogą zajść kolizję.
	 * @return Kolekcję przewidywanych, możliwych kolizji.
	 */
	public Collection<Collision> checkCollisionsWithBall(Ball ball)
	{
		Collection<Collision> collisionsList = new TreeSet<>();
		for (Brick brick : bricksList)
		{
			Collection<Collision> collisionsWithBrick = brick.checkCollisionsWithBall(ball);
			collisionsList.addAll(collisionsWithBrick);
		}		
		return collisionsList;
	}

	/**
	 * @return Prawdę, jeżeli wszystkie klocki zostały zniszczone; fałsz wpp.
	 */
	public boolean allBricksDestroyed()
	{
		return bricksList.isEmpty();
	}
	
	/**
	 * Dodaje do kolekcji klocków wszystkie klocki
	 * z podanej listy.
	 * 
	 * @param bricksList - lista klocków, które chcemy dodać.
	 */
	public void addAllBricks(List<Brick> bricksList)
	{
		for (Brick brick : bricksList)
		{
			this.bricksList.add(new Brick(brick));
		}
	}
	
	/**
	 * @return Listę klocków.
	 */
	public List<Brick> getBricksList()
	{
		return bricksList;
	}
}
