/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * Abstrakcyjna klasa reprezentująca 
 * strategię wykonywaną podczas odpowiedniej
 * kolizji.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
abstract class CollisionStrategy
{
	/**
	 * Metoda, która odpowiednio reaguje na kolizje
	 * i zmienia stan modelu (obiektór w modelu).
	 * 
	 * @param collision - kolizja.
	 */
	public abstract void handle(final Collision collision);
}
