/**
 * 
 */
package com.szuppe.jakub.model;

/**
 * Wyjątek informujący o ostatecznej śmierci gracza.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
public class PlayerDiedException extends Exception
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -4637864075698519809L;
}
