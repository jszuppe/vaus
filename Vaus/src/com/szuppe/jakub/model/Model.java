/**
 * 
 */
package com.szuppe.jakub.model;

import java.net.URL;

import com.szuppe.jakub.common.Direction;
import com.szuppe.jakub.config.model.ModelConfig;
import com.szuppe.jakub.mockups.BallMockup;
import com.szuppe.jakub.mockups.BricksMockup;
import com.szuppe.jakub.mockups.GameMockup;
import com.szuppe.jakub.mockups.PaddleMockup;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;

/**
 * Model gry "<b>Vaus</b>" - klona popularnej gry arkanoid.<br/>
 * <p>
 * Model po operacji {@link #update()} lub {@link #recalculateValidTime()} zawsze zawiera
 * poprawny stan aplikacji.
 * </p>
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class Model
{
	/** Plansza. Cała rozgrywa dzieje się właśnie na planszy. */
	private Level				level;
	/** Stan modelu: w menu lub w grze. */
	private ModelState			modelState;
	/** Konfiguracja modelu. */
	private final ModelConfig	modelConfig;

	/**
	 * Tworzy odpowiedni model świata gry na podstawie domyślnej konfiguracji.
	 * 
	 */
	public Model()
	{
		try
		{
			XStream xStream = new XStream();
			// Wczytuje domyślny plik konfiguracyjny
			URL urlToModelConfig = getClass().getResource(
					"/com/szuppe/jakub/resources/modelConfig.xml");
			this.modelConfig = (ModelConfig) xStream.fromXML(urlToModelConfig);
		} catch (XStreamException | NullPointerException e)
		{
			// Brak pliku konfiguracyjnego lub błąd podczas wczytywania.
			// Obecnie wyjątek powoduje RuntimeException.
			// W przyszłości może powodować wygenerowanie standardowych ustawień(?)
			throw new RuntimeException(e);
		}
	}

	/**
	 * Wykonuje obliczenia stanu modelu.
	 * 
	 * @throws AllBricksDestroyedException, PaddleDiedException
	 */
	public void update() throws AllBricksDestroyedException, PlayerDiedException
	{
		level.update();
	}

	public void recalculateValidTime() throws AllBricksDestroyedException,
			PlayerDiedException
	{
		level.recalculateValidTime();
	}

	/**
	 * Powoduje odczepienie się piłeczki od paletki.
	 */
	public void releaseBall()
	{
		level.releaseBall();
	}

	/**
	 * Rozpoczyna grę. Przechodzi z menu do gry.
	 */
	public void startGame()
	{
		this.level = new Level(modelConfig.getLevelConfig());
		modelState = ModelState.GAME;
	}

	/**
	 * Przechodzi do następnej mapy i ustawia .
	 * 
	 * @throws NoMoreMapsException
	 */
	public void loadNextMap() throws NoMoreMapsException
	{
		level.loadNextBricksMap();
	}

	/**
	 * Kończy grę. Powoduje powrót do menu.
	 */
	public void endGame()
	{
		modelState = ModelState.MENU;
		level = null;		
	}

	/**
	 * @return Prawdę, jeżeli gra jest rozpoczęta; fałsz wpp.
	 */
	public boolean gameStarted()
	{
		return (modelState == ModelState.GAME);
	}

	/**
	 * @return Prawdę, jeżeli piłka jest odłączona od paletki; fałsz wpp.
	 */
	public boolean ballIsBouncing()
	{
		return level.ballIsBouncing();
	}

	/**
	 * @return Makietę zawierającą informacje o stanie wszystkich elementów gry.
	 */
	public GameMockup getGameMockup()
	{
		return level.getGameMockup();
	}

	/**
	 * @return Makietę zawierająca informacje o stanie paletki.
	 */
	public PaddleMockup getPaddleMockup()
	{
		return level.getPaddleMockup();
	}

	/**
	 * @return Makietę zawierającą informację o stanie klocków.
	 */
	public BricksMockup getBricksMockup()
	{
		return level.getBricksMockup();
	}

	/**
	 * @return Makietę zawierając informację o stanie piłeczki.
	 */
	public BallMockup getBallMockup()
	{
		return level.getBallMockup();
	}

	/**
	 * Zwraca czas przez, który model będzie zawierał poprawny stan.<br/>
	 * <br/>
	 * <b>UWAGA:</b> Czas ten jest poprawny tylko tuż po wykonaniu operacji
	 * {@link #update()} lub {@link #recalculateValidTime()}.
	 * 
	 * @return Czas przez, który model będzie zawierał poprawny stan.
	 */
	public long getValidTime()
	{
		return level.getValidTime();
	}
	
	/**
	 * @return Prawdę jeżeli stan modelu jest ważny bez terminu czasowego; fałsz wpp.
	 */
	public boolean isValidWithoutTimeLimit()
	{
		return (level.getValidTime() == -1);
	}

	/**
	 * Wykonuje poruszenie paletką w odpowiednią stronę.
	 * 
	 * @param direction - kierunek, w którym należy poruszyć paletką.
	 */
	public void acceleratePaddle(Direction direction)
	{
		level.acceleratePaddle(direction);
	}
}

/**
 * Enum opisujący stan modelu.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
enum ModelState
{
	MENU, GAME
}
