/**
 * 
 */
package com.szuppe.jakub;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.szuppe.jakub.model.Model;
import com.szuppe.jakub.controller.Controller;
import com.szuppe.jakub.view.View;
import com.szuppe.jakub.events.AppEvent;

/**
 * <h3>Vaus Game</h3>
 * <br/>        
 * <p>Gra <i>Vaus</i> jest klonem znanej arcade'owej gry Arkanoid.</p>
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 */
public class Main
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// Kolejka blokująca dla widoku i kontrolera.
		final BlockingQueue<AppEvent> eventsBlockingQueue = new LinkedBlockingQueue<AppEvent>();
		// model
		final Model model = new Model();
		// widok
		final View view = new View(eventsBlockingQueue);
		// kontroler
		final Controller controller = new Controller(model, view, eventsBlockingQueue);
		// Uruchomienie.
		controller.init();
	}
}
