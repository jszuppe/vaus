/**
 * 
 */
package com.szuppe.jakub.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import com.szuppe.jakub.common.Acceleration2D;
import com.szuppe.jakub.common.Coordinates2D;
import com.szuppe.jakub.common.SpeedVector2D;
import com.szuppe.jakub.config.view.PaddleConfig;
import com.szuppe.jakub.mockups.PaddleMockup;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
class Paddle extends JComponent
{
	private static final long	serialVersionUID	= 8519727767035109635L;
	/** Współrzędne lewego-górnego rogu. */
	private Coordinates2D		topLeftCoordinates;
	/** Prędkość. */
	private SpeedVector2D		speed;
	/** Przyspieszenie. */
	private Acceleration2D		acceleration;
	/** Ostatni czas rysowania obiektu. */
	private long				lastRepaintTime;
	/** Obraz paletki. */
	private Image				paddleImage;
	/** Konfiguracja. */
	@SuppressWarnings("unused")
	private final PaddleConfig	paddleConfig;

	public Paddle(PaddleConfig paddleConfig, PaddleMockup paddleMockup)
	{
		this.paddleConfig = paddleConfig;
		
		setDoubleBuffered(paddleConfig.isDoubleBuffered());
		setVisible(true);
		this.topLeftCoordinates = paddleMockup.getTopLeftCoordinates();
		this.speed = paddleMockup.getSpeed();
		this.acceleration = paddleMockup.getRetardation();
		lastRepaintTime = System.currentTimeMillis();

		try
		{
			URL url = getClass().getResource(paddleConfig.getPathToPaddleImg());
			paddleImage = ImageIO.read(url);
		} catch (IOException | IllegalArgumentException e) {
			// TODO: W przyszłości można autogenerować obrazek.
			throw new RuntimeException(e);
		}	
	}

	
	/**
	 * Rysuje paletkę.
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.drawImage(paddleImage, topLeftCoordinates.getIntX(), topLeftCoordinates.getIntY(), null);
	}

	/**
	 * Ustawia stan paletki zgodnie z makietą.
	 * 
	 * @param paddleMockup
	 */
	public void setFromMockup(PaddleMockup paddleMockup)
	{
		this.topLeftCoordinates = paddleMockup.getTopLeftCoordinates();
		this.speed = paddleMockup.getSpeed();
		this.acceleration = paddleMockup.getRetardation();
		lastRepaintTime = System.currentTimeMillis();
	}

	/**
	 * Przesuwa paletkę zgodnie czasem, który upłynął.
	 */
	public void move()
	{
		long timeInterval = System.currentTimeMillis() - lastRepaintTime;
		lastRepaintTime = System.currentTimeMillis();
		Coordinates2D paddleDisplacementVector;

		// s = v * t + (a * t^2)/2
		float xballDisplacement = (speed.getXSpeed() * timeInterval)
				+ (0.5f * acceleration.getxAcc() * timeInterval * timeInterval);
		float yballDisplacement = (speed.getYSpeed() * timeInterval)
				+ (0.5f * acceleration.getyAcc() * timeInterval * timeInterval);
		if (Math.abs(speed.getXSpeed()) < Math.abs(0.5f * acceleration.getxAcc() * timeInterval)
				|| Math.abs(speed.getYSpeed()) < Math.abs(0.5f * acceleration.getyAcc() * timeInterval))
		{
			speed.zero();
			acceleration.zero();
			paddleDisplacementVector = new Coordinates2D(0, 0);
		}
		else
		{
			paddleDisplacementVector = new Coordinates2D(xballDisplacement, yballDisplacement);
		}
		topLeftCoordinates = topLeftCoordinates.moveAlongVector(paddleDisplacementVector);
		SpeedVector2D speedChange = acceleration.countSpeedChange(timeInterval);
		speed.add(speedChange);
	}
}
