/**
 * 
 */
package com.szuppe.jakub.view;

import java.util.concurrent.BlockingQueue;

import javax.swing.JFrame;

import com.szuppe.jakub.config.view.MainFrameConfig;
import com.szuppe.jakub.events.AppEvent;
import com.szuppe.jakub.mockups.GameMockup;

/**
 * Główne i jedyne okno aplikacji.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
class MainFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long		serialVersionUID	= -7146322916032972917L;
	/** Panel reprezentujący plansze gry. */
	private GamePanel				gamePanel;
	/** Menu uruchamiana m.in. na starcie. */
	private final MenuPanel			menuPanel;
	/** Konfiguracja okna. */
	private final MainFrameConfig	mainFrameConfig;

	/**
	 * Tworzy okno na podstawie konfiguracji.
	 * 
	 * @param mainFrameConfig
	 * @param eventsBlockingQueue
	 */
	public MainFrame(final MainFrameConfig mainFrameConfig,
			final BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		this.mainFrameConfig = mainFrameConfig;
		setTitle(mainFrameConfig.getTitle());
		setResizable(mainFrameConfig.isResizable());
		setLocation(mainFrameConfig.getLocation());
		setDefaultCloseOperation(mainFrameConfig.getDefaultCloseOperation());

		this.menuPanel = new MenuPanel(mainFrameConfig.getMenuConfig(),
				eventsBlockingQueue);
		menuPanel.setVisible(true);

		getContentPane().add(menuPanel);
		pack();
	}

	/**
	 * Rozpoczyna grę. Przechodzi z menu do planszy gry.
	 * Ustawia grę zgodnie z makietą.
	 * 
	 * @param eventsBlockingQueue
	 * @param gameMockup - makieta stanu gry.
	 */
	public void startGame(final BlockingQueue<AppEvent> eventsBlockingQueue,
			final GameMockup gameMockup)
	{
		this.gamePanel = new GamePanel(mainFrameConfig.getGamePanelConfig(),
				eventsBlockingQueue, gameMockup);
		getContentPane().add(gamePanel);
		pack();
		gamePanel.start();
		menuPanel.setVisible(false);
		gamePanel.setVisible(true);
		gamePanel.requestFocus();
	}

	/**
	 * Kończy grę.
	 * 
	 * @param playerWon - Prawda jeżeli gracz wygrał, fałsz wpp.
	 */
	public void endGame(final boolean playerWon)
	{
		gamePanel.stop();
		gamePanel.setVisible(false);
		getContentPane().remove(gamePanel);
		pack();
		gamePanel = null;
		menuPanel.setVisible(true);
		menuPanel.requestFocus();
	}

	/**
	 * Przeładowuje całą plansze informacjami z makiety
	 * stanu gry. Następnie wykonuje {@link #revalidate()} na
	 * panelu gry, co jest czasochłonne.
	 * 
	 * @param correctedGameMockup
	 */
	public void setGameUsingMockup(GameMockup correctedGameMockup)
	{
		if (gamePanel != null)
		{
			gamePanel.setBall(correctedGameMockup.getBallMockup());
			gamePanel.setPaddle(correctedGameMockup.getPaddleMockup());
			gamePanel.setBricks(correctedGameMockup.getBricksMockup());			
			gamePanel.setPlayerInfo(correctedGameMockup.getPlayerMockup());
		}
	}
	
	/**
	 * Należy wykonać jeżeli w makieta
	 * dodaje nowe elementy do planszy.
	 * 
	 * @see JFrame#revalidate()
	 */
	public void revalidateGamePanel()
	{
		gamePanel.revalidate();
	}
}
