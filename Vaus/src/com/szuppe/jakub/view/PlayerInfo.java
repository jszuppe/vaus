/**
 * 
 */
package com.szuppe.jakub.view;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.JComponent;

import com.szuppe.jakub.mockups.PlayerMockup;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
public class PlayerInfo extends JComponent
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -7335842644987079767L;
	/** Życia, które pozostały graczowi. */
	private int					lifesLeft;
	/** Punkt, w którym zaczynamy pisać. */
	private final Point			startWritingPoint;

	/**
	 * @param lifesLeft - Życie na początku.
	 */
	public PlayerInfo(final int lifesLeft, final Dimension levelDimension)
	{
		this.lifesLeft = lifesLeft;
		this.startWritingPoint = new Point(10, (int) (levelDimension.getHeight()-10));
	}

	/**
	 * @param lifesLeft - Życie na początku.
	 */
	public PlayerInfo(final PlayerMockup playerMockup, final Dimension levelDimension)
	{
		this(playerMockup.getLivesLeft(), levelDimension);
	}

	/**
	 * Ustawia wyświetlana życie na podaną wartość.
	 * @param lifesLeft
	 */
	public void setLifesLeft(final int lifesLeft)
	{
		this.lifesLeft = lifesLeft;
	}

	/**
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setFont(new Font("Sans", Font.PLAIN, 18));
		g2d.drawString("LIVES: " + lifesLeft, (int) startWritingPoint.getX(),
				(int) startWritingPoint.getY());
	}
}
