/**
 * 
 */
package com.szuppe.jakub.view;

/**
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
enum ViewState
{
	START_MENU,
	GAME;
}
