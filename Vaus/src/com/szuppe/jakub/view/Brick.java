/**
 * 
 */
package com.szuppe.jakub.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import com.szuppe.jakub.common.Coordinates2D;
import com.szuppe.jakub.mockups.BrickMockup;
import com.szuppe.jakub.mockups.BrickType;

/**
 * Klasa reprezentująca klocek w widoku.
 * Dziedziczy po {@link JComponent}.
 * Zawiera pozycje, typ oraz dany czy
 * należy repaint'ować klocek lub go usunąć.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
class Brick extends JComponent
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6125901106937178716L;
	/** Współrzędne lewego-górnego rogu. */
	private final Coordinates2D	topLeftCoordinates;
	/** Typ klocka. */
	private final BrickType		brickType;
	/** Wyświetlany obrazek klocka. */
	private Image				brickImage;
	/** Czy należy wykonać repaint */
	private boolean				needsRepaint;
	/** Czy należy usunąć. */
	private boolean				isToDelete;

	/**
	 * Tworzy nowy {@link Brick} na podstawie podanej
	 * pozycji, typu oraz ścieżki do zdj.
	 * 
	 * @param topLeftCoordinates - współrzędne lewego-górnego rogu.
	 * @param brickType - typ klocka.
	 */
	public Brick(final Coordinates2D topLeftCoordinates, final BrickType brickType,
			final String pathToImg)
	{
		setVisible(true);
		this.topLeftCoordinates = topLeftCoordinates;
		this.brickType = brickType;
		this.needsRepaint = true;
		this.isToDelete = false;

		try
		{
			URL url = getClass().getResource(pathToImg);
			brickImage = ImageIO.read(url);
		} catch (IOException | IllegalArgumentException e)
		{
			// Brak zdjęcia klocka
			// TAutogeneracja jakiegoś prostego zdj w razie braku pliku
			throw new RuntimeException(e);
		}
	}

	/**
	 * Tworzy nowy klocek na podstawie makiety
	 * klocka z modelu oraz ścieżki do obrazu klocka.
	 * 
	 * @param brickMockup - makieta klocka.
	 * @param pathToImg - ścieżka do obrazu klocka.
	 */
	public Brick(final BrickMockup brickMockup, final String pathToImg)
	{
		this(brickMockup.getPosition(), brickMockup.getBrickType(), pathToImg);
	}

	/**
	 * Rysuje klocek.
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		this.needsRepaint = false;
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2.drawImage(brickImage, topLeftCoordinates.getIntX(),
				topLeftCoordinates.getIntY(), null);
	}

	/**
	 * Ustawia, czy należy wykonać repaint klocka.
	 * 
	 * @param needsRepaint
	 */
	public void setNeedsRepaint(boolean needsRepaint)
	{
		this.needsRepaint = needsRepaint;
	}

	/**
	 * Ustawia klocek na "do usunięcia".
	 */
	public void delete()
	{
		this.setVisible(false);
		this.needsRepaint = true;
		this.isToDelete = true;
	}

	/**
	 * @return Prawdę, jeżeli klocek należy usunąć; fałsz wpp.
	 */
	public boolean isToDelete()
	{
		return isToDelete;
	}

	/**
	 * @return Prawdę, jeżeli trzeba wykonać repaint; fałsz wpp.
	 */
	public boolean needsRepaint()
	{
		return needsRepaint;
	}

	/**
	 * @return Współrzędne lewego-górnego rogu.
	 */
	public Coordinates2D getPosition()
	{
		return topLeftCoordinates;
	}

	/**
	 * @return Typ klocka.
	 */
	public BrickType getBrickType()
	{
		return brickType;
	}
}
