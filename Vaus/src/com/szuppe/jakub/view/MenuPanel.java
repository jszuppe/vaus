/**
 * 
 */
package com.szuppe.jakub.view;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.BlockingQueue;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import com.szuppe.jakub.config.view.MenuPanelConfig;
import com.szuppe.jakub.events.AppEvent;
import com.szuppe.jakub.events.StartGameEvent;


/**
 * Menu.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 *
 */
class MenuPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1296465394327168752L;
	@SuppressWarnings("unused")
	private final MenuPanelConfig menuPanelConfig;
	private Image bgImage;
	
	public MenuPanel(MenuPanelConfig menuPanelConfig, BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		this.menuPanelConfig = menuPanelConfig;
		setPreferredSize(menuPanelConfig.getPreferredSize());
		setBackground(menuPanelConfig.getBgColor());		
		initActions(eventsBlockingQueue);		
		try {
			URL url = getClass().getResource(menuPanelConfig.getPathToBackgroundImage());
			this.bgImage =  ImageIO.read(url);					
		} catch (IOException | IllegalArgumentException e) {
			// TODO: W przyszłości można autogenerować obrazek.
			throw new RuntimeException(e);
		}	
	}

	/** 
	 * Paint all page.
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);	
		if(bgImage != null)
		{
			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.drawImage(bgImage, 0, 0, null);
		}
	}
	
	/**
	 * @param eventsBlockingQueue
	 */
	@SuppressWarnings("serial")
	private void initActions(final BlockingQueue<AppEvent> eventsBlockingQueue)
	{
		getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "startGame");
		getActionMap().put("startGame", new AbstractAction()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					eventsBlockingQueue.put(new StartGameEvent());
				} catch (InterruptedException e1)
				{
					e1.printStackTrace();
				}
			}
		});
	}
}
