/**
 * 
 */
package com.szuppe.jakub.view;

import java.awt.Container;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import com.szuppe.jakub.config.view.BricksConfig;
import com.szuppe.jakub.mockups.BrickMockup;
import com.szuppe.jakub.mockups.BricksMockup;

/**
 * Zbiór wyświetlanych klocków.
 * 
 * @author Jakub Szuppe <j.szuppe at gmail.com>
 * 
 */
class Bricks
{
	/** Lista klocków. */
	private final List<Brick>	bricksList;
	/** Kontener, na którym rysowane są klocki. */
	private final Container		containter;
	/** Konfiguracja zawierająca informacje o ścieżkach zdj klocków. */
	private final BricksConfig	bricksConfig;
	/** Semafor dostępu do listy klocków. */
	private final Semaphore		bricksListMutex;

	/**
	 * Tworzy zbiór klocków na podstawie
	 * makiety, konfiguracji oraz kontenera, na którym
	 * rysowane będą klocki.
	 * 
	 * @param bricksConfig
	 * @param bricksMockup
	 * @param containter
	 */
	Bricks(final BricksConfig bricksConfig, BricksMockup bricksMockup,
			Container containter)
	{
		this(bricksConfig, bricksMockup.getBricksList(), containter);
	}

	/**
	 * Tworzy zbiór klocków na podstawie
	 * listy makiet klocków, konfiguracji oraz kontenera, na którym
	 * rysowane będą klocki.
	 * 
	 * @param bricksConfig
	 * @param brickMockups
	 * @param containter
	 */
	Bricks(final BricksConfig bricksConfig, List<BrickMockup> brickMockups,
			Container containter)
	{
		this.bricksConfig = bricksConfig;
		this.bricksListMutex = new Semaphore(1);
		this.bricksList = new LinkedList<>();
		this.containter = containter;
		Brick brick = null;
		for (BrickMockup brickMockup : brickMockups)
		{
			brick = new Brick(brickMockup, bricksConfig.getPathToBrickImg(brickMockup
					.getBrickType()));
			containter.add(brick);
			bricksList.add(brick);
		}
	}

	/**
	 * Usuwa klocki, które nie występują już w makiecie,
	 * a dodaje jeżeli są nowe.	 
	 *
	 * @param bricksMockup
	 */
	public void setFromMockup(BricksMockup bricksMockup)
	{
		try
		{
			bricksListMutex.acquire();
			List<BrickMockup> brickMockups = bricksMockup.getBricksList();
			for (Brick brick : bricksList)
			{
				brick.setNeedsRepaint(true);
				for (BrickMockup brickMockup : brickMockups)
				{
					if (brick.getPosition().equals(brickMockup.getPosition())
							&& brick.getBrickType().equals(brickMockup.getBrickType()))
					{
						brick.setNeedsRepaint(false);
					}
				}
				if (brick.needsRepaint())
				{
					brick.delete();
				}
			}
			for (BrickMockup brickMockup : brickMockups)
			{
				boolean newBrick = true;
				for (Brick brick : bricksList)
				{
					if (brick.getPosition().equals(brickMockup.getPosition())
							&& brick.getBrickType().equals(brickMockup.getBrickType()))
					{
						newBrick = false;
					}
				}
				if (newBrick)
				{
					Brick brick = new Brick(brickMockup,
							bricksConfig.getPathToBrickImg(brickMockup.getBrickType()));
					containter.add(brick);
					bricksList.add(brick);
				}
			}
			bricksListMutex.release();
		} catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Wywołuje repaint() na klockach, 
	 * które tego wymagają. Dzięki temu nie 
	 * odświeżamy klocków, które tego nie wymagają.
	 */
	public void repaint()
	{
		try
		{
			bricksListMutex.acquire();

			List<Brick> bricksToDelete = new LinkedList<>();
			for (Brick brick : bricksList)
			{
				if (brick.needsRepaint())
				{
					brick.repaint();
					if (brick.isToDelete())
					{
						bricksToDelete.add(brick);
					}
				}
			}
			for (Brick brick : bricksToDelete)
			{
				containter.remove(brick);
				bricksList.remove(brick);
			}
			bricksListMutex.release();
		} catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}
}
